﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Teste.Services;

namespace Teste.Controllers {
    public class FilterBooksController : Controller {
        //Instancia do serviço de filtro
        private readonly FilterBooksService _filterBooksService;

        //construtor
        public FilterBooksController(FilterBooksService filterBooksService) {
            _filterBooksService = filterBooksService;
        }

        public IActionResult Index() {
            return View();
        }

        //tentativa de criação do filtro por isbn
        public async Task<IActionResult> KeywordFilter(int isbn) {
            //aqui faço uma busca utilizando o método criado no service, passando o isbn como parametro
            var result = await _filterBooksService.FindByKeywordAsync(isbn);

            return View(result);

        }

        //tentativa de criação do filtro por data
        public async Task<IActionResult> DateFilter(DateTime? minDate, DateTime? maxDate) {
            //verificação da data, onde se a minDate for falsa, ou seja, não informada pelo usuário, eu mesmo a atribui um valor, no caso a data 01/01/1950
            if (!minDate.HasValue) {
                minDate = new DateTime(1950, 1, 1);

            }
            //mesma verificação acima, porem aplicando a data atual a variavel maxDate
            if(!maxDate.HasValue) {
                maxDate = DateTime.Now;

            }

            //conversão dos valores DateTime para uma string
            ViewData["minDate"] = minDate.Value.ToString("yyyy-MM-dd");
            ViewData["maxDate"] = maxDate.Value.ToString("yyyy-MM-dd");

            //busca do range em questão por meio do metodo criado no service
            var result = await _filterBooksService.FindByDateAsync(minDate, maxDate); 
            return View(result);

        }
    }
}