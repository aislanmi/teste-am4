﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Teste.Models {
    public class Login {

        //atributos da entidade login (*verificar como fazer tela de login*)
        [Key]
        public int IdLogin { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "Usuário")]
        public string User { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Senha" )]
        public string Password { get; set; }

    }
}
