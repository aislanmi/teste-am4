﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Teste.Models;

namespace Teste.Models {
    public class Book {
        //atributos da entidade livro

        [DataType(DataType.Upload)]
        [Display(Name = "Imagem")]
        public byte[] Image { get; set; }

        [Required]
        [Display(Name = "Titulo")]
        public string Title { get; set; }

        [Key] //definição do isbn como pk
        [Required]
        [Display(Name = "ISBN")]
        public int Isbn { get; set; }

        [Display(Name = "Editora")]
        public string PublishingCompany { get; set; }

        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Display(Name = "Sinopse")]
        public string Synopsis { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data da publicação")]
        public DateTime PublicationDate { get; set; }

        //public ICollection<FilterBook> Filter { get; set; } = new List<FilterBook>();

        //contrutores
        public Book() {
        }

        public Book(byte[] image, string title, int isbn, string publishingCompany, string author, string synopsis, DateTime publicationDate) {
            Image = image;
            Title = title;
            Isbn = isbn;
            PublishingCompany = publishingCompany;
            Author = author;
            Synopsis = synopsis;
            PublicationDate = publicationDate;
        }
    }
}
