﻿using Teste.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Teste.Models {
    public class FilterBook {
        //atributos da entidade filter book (acho que to fazendo errado)

        [DataType(DataType.Upload)]
        [Display(Name = "Imagem")]
        public byte[] Image { get; set; }

        [Required]
        [Display(Name = "Titulo da obra")]
        public string Title { get; set; }

        [Key]
        [Required]
        [Display(Name = "ISBN")]
        public int Isbn { get; set; }

        [Display(Name = "Editora")]
        public string PublishingCompany { get; set; }

        [Display(Name = "Autor")]
        public string Author { get; set; }

        [Display(Name = "Sinopse")]
        public string Synopsis { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data da publicação")]
        public DateTime PublicationDate { get; set; }
        public Book Book { get; set; }

        //construtores
        public FilterBook() {
        }

        public FilterBook(byte[] image, string title, int isbn, string publishingCompany, string author, string synopsis, DateTime publicationDate, Book book) {
            Image = image;
            Title = title;
            Isbn = isbn;
            PublishingCompany = publishingCompany;
            Author = author;
            Synopsis = synopsis;
            PublicationDate = publicationDate;
            Book = book;
        }
    }
}
