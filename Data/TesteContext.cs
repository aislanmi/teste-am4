﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Teste.Models;

namespace Teste.Data {
    public class TesteContext : DbContext {

        //criação e configurações da conexão com o bd
        public TesteContext(DbContextOptions<TesteContext> options)
            : base(options) {
        }

        //adicionando os models ao bd
        public DbSet<Book> Book { get; set; }
        public DbSet<FilterBook> FilterBook { get; set; }
    }
}
