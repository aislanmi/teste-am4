﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Teste.Data;
using Teste.Models;

namespace Teste.Services {
    public class FilterBooksService {

        //chamada ao db
        private readonly TesteContext _context;

        //construtor
        public FilterBooksService(TesteContext context) {
            _context = context;
        }

        //tentativa de implementar a tratativa do filtro de data por range
        public async Task<List<FilterBook>> FindByDateAsync(DateTime? minDate, DateTime? maxDate) {
            //aqui seleciona um objeto no model FilterBook
            var result = from obj in _context.FilterBook select obj;

            return await result
                .OrderByDescending(x => x.PublicationDate) //ordenação
                .ToListAsync(); //transforma em lista

        }

        //tentativa de implementar a tratativa do filtro de ISBN
        public async Task<List<FilterBook>> FindByKeywordAsync(int isbn) {
            //aqui seleciona um objeto no model FilterBook
            var result = from obj in _context.FilterBook select obj;

            //com esse obj, faz uma busca do isbn vindo como parametro do método e o compara com os armazenados no bd
            result.Where(x => x.Isbn == isbn).Select(x => x.Isbn);
            
            return await result
                .OrderByDescending(x => x.PublicationDate)
                .ToListAsync();


        }
    }
}
